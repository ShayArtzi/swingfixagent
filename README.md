# Swing Fix Agent
A quick fix to the Hebrew rendering performance issue in Java Swing

## How to use
1. Make sure Java and Maven are installed on your machine
1. Clone the repo
1. Build and pack using maven (cd into the swing-fix-agent dir and run `mvn package`)
1. Run the test application:
    * Without the agent: `java -cp swing-fix-agent-0.0.1-SNAPSHOT.jar com.kipodopik.sfa.dummyapp.DummyApp`
    * With the agent: `java -javaagent:swing-fix-agent-0.0.1-SNAPSHOT.jar -cp swing-fix-agent-0.0.1-SNAPSHOT.jar com.kipodopik.sfa.dummyapp.DummyApp`